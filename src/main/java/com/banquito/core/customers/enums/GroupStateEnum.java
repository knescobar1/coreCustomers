package com.banquito.core.customers.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum GroupStateEnum {
  ACTIVE("ACT", "Active"),
  INACTIVE("INA", "Inactive"),
  CLOSED("CLO", "Closed");

  private final String value;
  private final String text;
}

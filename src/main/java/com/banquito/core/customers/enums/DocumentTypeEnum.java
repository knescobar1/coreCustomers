package com.banquito.core.customers.enums;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum DocumentTypeEnum {
  CED("CED", "Cedula"),
  RUC("RUC", "RUC");

  private final String value;
  private final String text;
}

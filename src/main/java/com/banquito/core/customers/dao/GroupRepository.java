package com.banquito.core.customers.dao;

import com.banquito.core.customers.model.Group;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepository extends MongoRepository<Group, String> {

  List<Group> findByEmailOrderByName(String email);

  List<Group> findByStateOrderByName(String state);

  Optional<Group> findByInternalId(String internalId);

  Optional<Group> findByDocummentTypeAndDocummentId(String documentType, String documentId);
}

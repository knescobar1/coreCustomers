package com.banquito.core.customers.dao;

import com.banquito.core.customers.model.Customer;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {

  Optional<Customer> findByDocumentTypeAndDocumentId(String documentType, String documentId);

  Optional<Customer> findByInternalId(String internalId);
}

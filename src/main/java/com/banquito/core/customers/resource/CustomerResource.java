package com.banquito.core.customers.resource;

import com.banquito.core.customers.dto.CustomerDTO;
import com.banquito.core.customers.mapper.CustomerMapper;
import com.banquito.core.customers.model.Customer;
import com.banquito.core.customers.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/customer")
@RequiredArgsConstructor
public class CustomerResource {
  private final CustomerService service;

  @PostMapping
  public ResponseEntity<CustomerDTO> create(@RequestBody CustomerDTO dto) {
    try {
      Customer customer = this.service.createCustomer(CustomerMapper.buildCustomer(dto));
      return ResponseEntity.ok(CustomerMapper.buildCustomerDTO(customer));
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @PutMapping
  public ResponseEntity<Customer> update(@RequestBody Customer customer) {
    try {
      this.service.updateCustomer(customer);
      customer =
          this.service.obtainByDocumentTypeAndDocumentId(
              customer.getDocumentType(), customer.getDocumentId());
      return ResponseEntity.ok(customer);
    } catch (Exception e) {
      e.printStackTrace();
      return ResponseEntity.badRequest().build();
    }
  }

  @GetMapping(path = "/{internalId}")
  public ResponseEntity<CustomerDTO> findByInternalId(
      @PathVariable("internalId") String internalId) {
    try {
      Customer customer = this.service.obtainByInternalId(internalId);
      return ResponseEntity.ok(CustomerMapper.buildCustomerDTO(customer));
    } catch (Exception e) {
      return ResponseEntity.notFound().build();
    }
  }
}

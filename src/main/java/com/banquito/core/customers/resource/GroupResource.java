package com.banquito.core.customers.resource;

import com.banquito.core.customers.dto.GroupDTO;
import com.banquito.core.customers.enums.GroupStateEnum;
import com.banquito.core.customers.mapper.GroupMapper;
import com.banquito.core.customers.model.Group;
import com.banquito.core.customers.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/groups")
@RequiredArgsConstructor
public class GroupResource {
  private final GroupService groupService;

  @GetMapping
  public ResponseEntity<List<GroupDTO>> findAllActiveGroups() {
    List<GroupDTO> groupDTOs =
        this.groupService.findByState(GroupStateEnum.ACTIVE).stream()
            .map(GroupMapper::buildGroupDTO)
            .collect(Collectors.toList());

    return ResponseEntity.ok(groupDTOs);
  }

  @GetMapping("/{internalId}")
  public ResponseEntity<GroupDTO> findByInternalId(@PathVariable String internalId) {
    Group group = this.groupService.findByInternalId(internalId);
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @GetMapping("/ruc/{ruc}")
  public ResponseEntity<GroupDTO> findByRUC(@PathVariable String ruc) {
    Group group = this.groupService.findByRUC(ruc);
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @GetMapping("/search")
  public ResponseEntity<List<GroupDTO>> findAllGroupsByEmail(@PathParam("email") String email) {
    List<GroupDTO> groupDTOs =
        this.groupService.findByEmail(email).stream()
            .map(GroupMapper::buildGroupDTO)
            .collect(Collectors.toList());

    return ResponseEntity.ok(groupDTOs);
  }

  @PostMapping
  public ResponseEntity<GroupDTO> create(@RequestBody GroupDTO dto) {
    Group group = this.groupService.create(GroupMapper.buildGroup(dto));
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @PutMapping("/{internalId}")
  public ResponseEntity<GroupDTO> update(
      @PathVariable String internalId, @RequestBody GroupDTO dto) {
    Group group = this.groupService.update(internalId, GroupMapper.buildGroup(dto));
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @PutMapping("/{internalId}/activate")
  public ResponseEntity<GroupDTO> activateGroup(@PathVariable String internalId) {
    Group group = this.groupService.activateGroup(internalId);
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @PutMapping("/{internalId}/deactivate")
  public ResponseEntity<GroupDTO> deactivateGroup(@PathVariable String internalId) {
    Group group = this.groupService.deactivateGroup(internalId);
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }

  @PutMapping("/{internalId}/close")
  public ResponseEntity<GroupDTO> closeGroup(@PathVariable String internalId) {
    Group group = this.groupService.closeGroup(internalId);
    return ResponseEntity.ok(GroupMapper.buildGroupDTO(group));
  }
}

package com.banquito.core.customers.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerAddress {

  private String country;
  private String region;
  private String city;
  private String line1;
  private String line2;
  private String postalCode;
  private String type;
  private Boolean primary;
}

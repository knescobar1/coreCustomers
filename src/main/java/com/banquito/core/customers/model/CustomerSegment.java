package com.banquito.core.customers.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerSegment {

  private String code;

  private String name;
}

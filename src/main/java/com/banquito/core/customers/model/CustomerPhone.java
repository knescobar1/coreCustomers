package com.banquito.core.customers.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerPhone {

  private String phone;

  private String type;

  private Boolean primary;
}

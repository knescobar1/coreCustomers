package com.banquito.core.customers.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document(collection = "groups")
@TypeAlias("groups")
@CompoundIndex(name = "idxco_group_docTypeDocId", def = "{'docummentType': 1, 'docummentId': 1}")
public class Group {

  @Id private String id;

  @Indexed(name = "idxu_group_internalId", unique = true)
  private String internalId;

  private String docummentType;

  private String docummentId;

  private String name;

  @Indexed(name = "idx_group_email", unique = false)
  private String email;

  private CustomerSegment segment;

  private String assignedBranch;

  private Date creationDate;

  private Date lastModifiedDate;

  @Indexed(name = "idx_group_state", unique = false)
  private String state;

  private Date closedDate;

  private List<CustomerPhone> phones;

  private List<CustomerAddress> addresses;

  private List<GroupMember> groupMembers;
}

package com.banquito.core.customers.model;

import lombok.Data;

@Data
public class GroupMember {

  private String customerKey;

  private String role;
}

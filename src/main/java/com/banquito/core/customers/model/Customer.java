package com.banquito.core.customers.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Builder
@Document(collection = "customers")
@TypeAlias("customers")
@CompoundIndex(name = "idxco_customer_docTypeDocId", def = "{'docummentType': 1, 'docummentId': 1}")
public class Customer {

  @Id private String id;

  @Indexed(name = "idxu_customer_internalId", unique = true)
  private String internalId;

  private String documentType;

  private String documentId;

  private String firstName;

  private String midName;

  @Indexed(name = "idx_customer_lastName", unique = false)
  private String lastName;

  @Indexed(name = "idx_customer_fullName", unique = false)
  private String fullName;

  private Date birthDate;

  private String gender;

  @Indexed(name = "idx_customer_email", unique = false)
  private String email;

  private CustomerSegment segment;

  private String assignedBranch;

  private Date creationDate;

  private Date lastModifiedDate;

  @Indexed(name = "idx_customer_state", unique = false)
  private String state;

  private Date closedDate;

  private List<CustomerPhone> phones;

  private List<CustomerAddress> addresses;

  private String signatureHash;
}

package com.banquito.core.customers.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PhoneDTO {

  private String type;
  private String phone;
  private Boolean primary;
}

package com.banquito.core.customers.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerSegmentDTO {

  private String code;
  private String name;
}

package com.banquito.core.customers.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressDTO {

  private String type;
  private String country;
  private String region;
  private String city;
  private String line1;
  private String line2;
  private String postalCode;
  private Boolean primary;
}

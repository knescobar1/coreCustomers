package com.banquito.core.customers.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class GroupDTO {

  private String internalId;

  private String documentType;

  private String documentId;

  private String name;

  private String email;

  private CustomerSegmentDTO segment;

  private String assignedBranch;

  private Date creationDate;

  private String state;

  private List<PhoneDTO> phones;

  private List<AddressDTO> addresses;
}

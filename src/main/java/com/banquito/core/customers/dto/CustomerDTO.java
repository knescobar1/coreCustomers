package com.banquito.core.customers.dto;

import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDTO {

  private String internalId;

  private String documentType;

  private String documentId;

  private String firstName;

  private String midName;

  private String lastName;

  private String fullName;

  private Date birthDate;

  private String gender;

  private String email;

  private CustomerSegmentDTO segment;

  private String assignedBranch;

  private Date creationDate;

  private String state;

  private List<AddressDTO> addresses;

  private List<PhoneDTO> phones;
}

package com.banquito.core.customers.mapper;

import com.banquito.core.customers.dto.PhoneDTO;
import com.banquito.core.customers.model.CustomerPhone;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PhoneMapper {

  public static CustomerPhone buildCustomerPhone(PhoneDTO dto) {
    return CustomerPhone.builder()
        .primary(dto.getPrimary())
        .phone(dto.getPhone())
        .type(dto.getType())
        .build();
  }

  public static PhoneDTO buildPhoneDTO(CustomerPhone phone) {
    return PhoneDTO.builder()
        .primary(phone.getPrimary())
        .phone(phone.getPhone())
        .type(phone.getType())
        .build();
  }
}

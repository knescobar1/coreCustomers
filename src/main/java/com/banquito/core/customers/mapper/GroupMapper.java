package com.banquito.core.customers.mapper;

import com.banquito.core.customers.dto.AddressDTO;
import com.banquito.core.customers.dto.GroupDTO;
import com.banquito.core.customers.dto.PhoneDTO;
import com.banquito.core.customers.model.CustomerAddress;
import com.banquito.core.customers.model.CustomerPhone;
import com.banquito.core.customers.model.Group;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GroupMapper {

  // TODO: Verificar todos los datos necesarios.
  public static Group buildGroup(GroupDTO dto) {
    List<CustomerAddress> addresses =
        dto.getAddresses().stream()
            .map(AddressMapper::buildCustomerAddress)
            .collect(Collectors.toList());

    List<CustomerPhone> phones =
        dto.getPhones().stream().map(PhoneMapper::buildCustomerPhone).collect(Collectors.toList());

    return Group.builder()
        .internalId(dto.getInternalId())
        .docummentType(dto.getDocumentType())
        .docummentId(dto.getDocumentId())
        .name(dto.getName())
        .email(dto.getEmail())
        .segment(CustomerSegmentMapper.buildCustomerSegment(dto.getSegment()))
        .assignedBranch(dto.getAssignedBranch())
        .phones(phones)
        .addresses(addresses)
        .build();
  }

  public static GroupDTO buildGroupDTO(Group group) {
    List<AddressDTO> addressDTOS =
        group.getAddresses().stream()
            .map(AddressMapper::buildAddressDTO)
            .collect(Collectors.toList());

    List<PhoneDTO> phoneDTOS =
        group.getPhones().stream().map(PhoneMapper::buildPhoneDTO).collect(Collectors.toList());

    return GroupDTO.builder()
        .internalId(group.getInternalId())
        .documentType(group.getDocummentType())
        .documentId(group.getDocummentId())
        .name(group.getName())
        .email(group.getEmail())
        .segment(CustomerSegmentMapper.buildCustomerSegmentDTO(group.getSegment()))
        .assignedBranch(group.getAssignedBranch())
        .phones(phoneDTOS)
        .addresses(addressDTOS)
        .build();
  }
}

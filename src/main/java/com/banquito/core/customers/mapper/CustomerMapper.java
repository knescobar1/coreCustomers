package com.banquito.core.customers.mapper;

import com.banquito.core.customers.dto.AddressDTO;
import com.banquito.core.customers.dto.CustomerDTO;
import com.banquito.core.customers.dto.PhoneDTO;
import com.banquito.core.customers.model.Customer;
import com.banquito.core.customers.model.CustomerAddress;
import com.banquito.core.customers.model.CustomerPhone;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerMapper {

  public static Customer buildCustomer(CustomerDTO dto) {
    Customer customer =
        Customer.builder()
            .documentType(dto.getDocumentType())
            .documentId(dto.getDocumentId())
            .firstName(dto.getFirstName())
            .midName(dto.getMidName())
            .lastName(dto.getLastName())
            .birthDate(dto.getBirthDate())
            .gender(dto.getGender())
            .email(dto.getEmail())
            .segment(CustomerSegmentMapper.buildCustomerSegment(dto.getSegment()))
            .assignedBranch(dto.getAssignedBranch())
            .build();

    List<CustomerPhone> phones = new ArrayList<>();
    for (PhoneDTO phoneDTO : dto.getPhones()) {
      phones.add(PhoneMapper.buildCustomerPhone(phoneDTO));
    }
    customer.setPhones(phones);

    List<CustomerAddress> addresses = new ArrayList<>();
    for (AddressDTO addressDTO : dto.getAddresses()) {
      addresses.add(AddressMapper.buildCustomerAddress(addressDTO));
    }
    customer.setAddresses(addresses);

    return customer;
  }

  public static CustomerDTO buildCustomerDTO(Customer customer) {
    CustomerDTO dto =
        CustomerDTO.builder()
            .documentType(customer.getDocumentType())
            .documentId(customer.getDocumentId())
            .firstName(customer.getFirstName())
            .midName(customer.getMidName())
            .lastName(customer.getLastName())
            .birthDate(customer.getBirthDate())
            .gender(customer.getGender())
            .email(customer.getEmail())
            .segment(CustomerSegmentMapper.buildCustomerSegmentDTO(customer.getSegment()))
            .assignedBranch(customer.getAssignedBranch())
            .build();

    List<PhoneDTO> phonesDTO = new ArrayList<>();
    for (CustomerPhone customerPhone : customer.getPhones()) {
      phonesDTO.add(PhoneMapper.buildPhoneDTO(customerPhone));
    }
    dto.setPhones(phonesDTO);

    List<AddressDTO> addressesDTO = new ArrayList<>();
    for (CustomerAddress customerAddress : customer.getAddresses()) {
      addressesDTO.add(AddressMapper.buildAddressDTO(customerAddress));
    }
    dto.setAddresses(addressesDTO);

    return dto;
  }
}

package com.banquito.core.customers.mapper;

import com.banquito.core.customers.dto.CustomerSegmentDTO;
import com.banquito.core.customers.model.CustomerSegment;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerSegmentMapper {

  public static CustomerSegment buildCustomerSegment(CustomerSegmentDTO dto) {
    return CustomerSegment.builder().code(dto.getCode()).name(dto.getName()).build();
  }

  public static CustomerSegmentDTO buildCustomerSegmentDTO(CustomerSegment customerSegment) {
    return CustomerSegmentDTO.builder()
        .code(customerSegment.getCode())
        .name(customerSegment.getName())
        .build();
  }
}

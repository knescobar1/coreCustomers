package com.banquito.core.customers.mapper;

import com.banquito.core.customers.dto.AddressDTO;
import com.banquito.core.customers.model.CustomerAddress;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AddressMapper {

  public static CustomerAddress buildCustomerAddress(AddressDTO dto) {
    return CustomerAddress.builder()
        .type(dto.getType())
        .country(dto.getCountry())
        .region(dto.getRegion())
        .city(dto.getCity())
        .line1(dto.getLine1())
        .line2(dto.getLine2())
        .postalCode(dto.getPostalCode())
        .type(dto.getType())
        .primary(dto.getPrimary())
        .build();
  }

  public static AddressDTO buildAddressDTO(CustomerAddress address) {
    return AddressDTO.builder()
        .type(address.getType())
        .country(address.getCountry())
        .region(address.getRegion())
        .city(address.getCity())
        .line1(address.getLine1())
        .line2(address.getLine2())
        .postalCode(address.getPostalCode())
        .type(address.getType())
        .primary(address.getPrimary())
        .build();
  }
}

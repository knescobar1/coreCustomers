package com.banquito.core.customers.service;

import com.banquito.core.customers.dao.GroupRepository;
import com.banquito.core.customers.enums.DocumentTypeEnum;
import com.banquito.core.customers.enums.GroupStateEnum;
import com.banquito.core.customers.exception.EntityNotFoundException;
import com.banquito.core.customers.model.Group;
import com.banquito.core.customers.model.GroupMember;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GroupService {
  private final GroupRepository groupRepository;

  public Group create(Group newGroup) {

    newGroup.setInternalId(UUID.randomUUID().toString());
    newGroup.setCreationDate(new Date());
    newGroup.setState(GroupStateEnum.ACTIVE.getValue());

    return this.groupRepository.save(newGroup);
  }

  public Group update(String internalId, Group group) {
    Group groupDB = this.findByInternalId(internalId);

    groupDB.setName(group.getName());
    groupDB.setEmail(group.getEmail());
    groupDB.setAddresses(group.getAddresses());
    groupDB.setPhones(group.getPhones());
    groupDB.setLastModifiedDate(new Date());

    return this.groupRepository.save(groupDB);
  }

  public void assignGroupMembers(String internalId, List<GroupMember> groupMembers) {
    Group groupDB = this.findByInternalId(internalId);

    groupDB.setGroupMembers(groupMembers);
    groupDB.setLastModifiedDate(new Date());

    this.groupRepository.save(groupDB);
  }

  public void assignGroupMember(String internalId, GroupMember groupMember) {
    Group groupDB = this.findByInternalId(internalId);

    groupDB.getGroupMembers().add(groupMember);
    groupDB.setLastModifiedDate(new Date());

    this.groupRepository.save(groupDB);
  }

  public void removeGroupMember(String internalId, GroupMember groupMember) {
    Group groupDB = this.findByInternalId(internalId);

    groupDB.getGroupMembers().remove(groupMember);
    groupDB.setLastModifiedDate(new Date());

    this.groupRepository.save(groupDB);
  }

  public Group activateGroup(String internalId) {
    return this.chanceGroupState(internalId, GroupStateEnum.ACTIVE);
  }

  public Group deactivateGroup(String internalId) {
    return this.chanceGroupState(internalId, GroupStateEnum.INACTIVE);
  }

  public Group closeGroup(String internalId) {
    return this.chanceGroupState(internalId, GroupStateEnum.CLOSED);
  }

  private Group chanceGroupState(String internalId, GroupStateEnum state) {
    Group groupDB = this.findByInternalId(internalId);
    groupDB.setState(state.getValue());
    groupDB.setLastModifiedDate(new Date());

    if (state.equals(GroupStateEnum.CLOSED)) {
      groupDB.setClosedDate(new Date());
    }

    return this.groupRepository.save(groupDB);
  }

  public Group findByRUC(String ruc) {
    return this.groupRepository
        .findByDocummentTypeAndDocummentId(DocumentTypeEnum.RUC.getValue(), ruc)
        .orElseThrow(
            () -> new EntityNotFoundException("No se ha encontrado un grupo con el ruc " + ruc));
  }

  public Group findByInternalId(String internalId) {
    return this.groupRepository
        .findByInternalId(internalId)
        .orElseThrow(
            () ->
                new EntityNotFoundException(
                    "No se ha encontrado un grupo con el id " + internalId));
  }

  public List<Group> findByEmail(String email) {
    return this.groupRepository.findByEmailOrderByName(email);
  }

  public List<Group> findByState(GroupStateEnum state) {
    return this.groupRepository.findByStateOrderByName(state.getValue());
  }
}

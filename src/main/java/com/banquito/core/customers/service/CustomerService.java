package com.banquito.core.customers.service;

import com.banquito.core.customers.dao.CustomerRepository;
import com.banquito.core.customers.enums.CustomerStateEnum;
import com.banquito.core.customers.exception.EntityNotFoundException;
import com.banquito.core.customers.model.Customer;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class CustomerService {


  private CustomerRepository customerRepository;

  public CustomerService(CustomerRepository customerRepository) {

    this.customerRepository = customerRepository;
  }

  public Customer createCustomer(Customer customer) {
    log.info("Va a crear un cliente con la siguiente informacion: {}", customer);
    Optional<Customer> customerOpt =
        this.customerRepository.findByDocumentTypeAndDocumentId(
            customer.getDocumentType(), customer.getDocumentId());

    if (customerOpt.isPresent()) {
      log.warn("El cliente {} ya existe. ", customer.getLastName());
      throw new EntityNotFoundException(
          "There's already a customer with this document type and id");
    }

    customer.setState(CustomerStateEnum.PENDING.getValue());
    customer.setCreationDate(new Date());
    customer.setLastModifiedDate(new Date());
    customer.setFullName(
        customer.getLastName()
            + " "
            + ((customer.getMidName() != null && !customer.getMidName().isBlank())
                ? (customer.getMidName() + " ")
                : "")
            + customer.getFirstName());
    customer.setInternalId(UUID.randomUUID().toString());
    this.customerRepository.save(customer);
    log.info("Se creo el cliente ", customer);
    return customer;
  }

  public void updateCustomer(Customer customer) {

    Optional<Customer> customerOpt =
        this.customerRepository.findByDocumentTypeAndDocumentId(
            customer.getDocumentType(), customer.getDocumentId());
    if (customerOpt.isEmpty()) {
      throw new EntityNotFoundException(
          "There is no customer with the document type and id entered");
    }

    Customer customerBD = customerOpt.get();
    customerBD.setFirstName(customer.getFirstName());
    customerBD.setMidName(customer.getMidName());
    customerBD.setLastName(customer.getLastName());
    customerBD.setFullName(customer.getFullName());
    customerBD.setGender(customer.getGender());
    customerBD.setEmail(customer.getEmail());
    customerBD.setAddresses(customer.getAddresses());
    customerBD.setPhones(customer.getPhones());
    customerBD.setState(customer.getState());
    customerBD.setLastModifiedDate(new Date());

    this.customerRepository.save(customerBD);
  }

  public Customer obtainByDocumentTypeAndDocumentId(String documentType, String documentId) {

    Optional<Customer> customerOpt =
        this.customerRepository.findByDocumentTypeAndDocumentId(documentType, documentId);
    if (customerOpt.isEmpty()) {
      throw new EntityNotFoundException(
          "There is no customer with the document type: "
              + documentType
              + " and document id: "
              + documentId);
    }

    return customerOpt.get();
  }

  public void changeCustomerState(Customer customer, CustomerStateEnum state) {
    customer.setState(state.getValue());
    customer.setLastModifiedDate(new Date());
    if (state.equals(CustomerStateEnum.CLOSED)) {
      customer.setClosedDate(new Date());
    }
    this.customerRepository.save(customer);
  }

  public void activateCustomer(String documentType, String documentId) {
    Customer customer = this.obtainByDocumentTypeAndDocumentId(documentType, documentId);
    this.changeCustomerState(customer, CustomerStateEnum.ACTIVE);
  }

  public void closeCustomer(String documentType, String documentId) {
    Customer customer = this.obtainByDocumentTypeAndDocumentId(documentType, documentId);
    this.changeCustomerState(customer, CustomerStateEnum.CLOSED);
  }

  public Customer obtainByInternalId(String internalId) {
    Optional<Customer> customerOpt = this.customerRepository.findByInternalId(internalId);
    return customerOpt.get();
  }
}
